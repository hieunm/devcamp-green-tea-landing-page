# Green Tea Landing Page
## 📄 Description User Page
 - ### **Home page Green Tea Landing Page**, giới thiệu cửa hàng, menu sản phẩm và  theo dõi các sự kiện của cửa hàng, **tạo mới, kiểm tra và gửi thông tin liên hệ đặt hàng**
## ✨ Feature
+ User portal ***Giao diện người dùng***
![Home page](<images/demo markdown/home_page.png>)
+ *Responsive*, Giao diện tương thích với các thiết bị di động
![Responsive](<images/demo markdown/responsive.png>)
+ Giới thiệu cửa hàng
![Introduce](<images/demo markdown/introduce.png>)
+ Lựa chọn sản phẩm theo menu có sẵn
![Menu](<images/demo markdown/menu.png>)
+ Theo dõi sát sao các sự kiện của cửa hàng
![Event](<images/demo markdown/discount.png>)
+ *Đặt hàng*, Thông tin liên hệ
![Dathang](<images/demo markdown/create_contact.png>)
+ *Đặt hàng*, Kiểm tra thông tin liên hệ
![Dathang](<images/demo markdown/validate_data_user.png>)
+ *Đặt hàng*, Ghi nhớ ngày gửi thông tin liên hệ
![Dathang](<images/demo markdown/create_date.png>)

## 📄 Description Admin Page
 - ### **Admin page Green Tea Landing Page**, Kiểm tra danh sách liên hệ, **chỉnh sửa và xóa thông tin liên hệ đặt hàng**
## ✨ Feature
+ Admin List Contact ***Trang quản lý danh sách liên hệ***
![List Contact](<images/demo markdown/list_contact_admin.png>)
+ Admin Contact Detail ***Trang thông tin chi tiết liên hệ***
![Contact Detail](<images/demo markdown/contact_detail_admin.png>)
+ *Thông tin liên hệ chi tiết*, Kiểm tra dữ liệu cập nhật
![Update](<images/demo markdown/validate_save_data_admin.png>)
+ *Thông tin liên hệ chi tiết*, Cập nhật và thông báo cho người dùng
![Update](<images/demo markdown/update_contact_admin.png>)
+ *Xóa thông tin liên hệ*, Có hỏi trước khi xóa
![Delete](<images/demo markdown/delete_contact_admin.png>)

## 🧱 Technology
+ Front-end:
 : 1.[Bootstrap 4](https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css)
 : 2.[Jquery 3](https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js)
 : 3.Ajax
 : 4.JSON
 : 5.Javascript
 : 6.Local Storage
+ Back-end:
 : Spring boot with Hibernate
+ KIT:
 : [AdminLTE](http://adminlte.io/)
+ DB:
 : MySQL - PhpMyAdmin